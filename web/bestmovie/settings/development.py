# settings/development.py

import socket

from .base import *  # noqa: F403

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# STATICFILES_DIRS = [  # noqa: F405
#     os.path.join(BASE_DIR, "_packaging/node_modules"),  # noqa: F405
# ]

ALLOWED_HOSTS = ["*"]

INTERNAL_IPS = [
    "127.0.0.1",
]

# workaround for debug_toolbar
ip = socket.gethostbyname(socket.gethostname())
INTERNAL_IPS += [ip[:-1] + "1"]

# Application definition
INSTALLED_APPS += [  # noqa: F405
    "debug_toolbar",
]

MIDDLEWARE += [  # noqa: F405
    "debug_toolbar.middleware.DebugToolbarMiddleware",  # noqa: F405
]


# Application definition
DEFAULT_FROM_EMAIL = get_env_variable("EMAIL_HOST_USER")  # noqa: F405
ADMINS = [("SpinForward", "sysadmin@spinforward.it")]
# https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = "mailhog-spinforward-bestmovie"
# https://docs.djangoproject.com/en/dev/ref/settings/#email-port
EMAIL_PORT = 1025
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
