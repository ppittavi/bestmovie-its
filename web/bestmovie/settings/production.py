# settings/production.py

from .base import *  # noqa: F403 to bypass pre-commit exeption

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

USE_X_FORWARDED_HOST = True
ALLOWED_HOSTS = ["*"]


INTERNAL_IPS = []
# Application definition
ADMINS = [("Spinforward Sysadmins", "sysadmin@spinforward.it")]
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = get_env_variable("EMAIL_HOST")  # noqa: F405
DEFAULT_FROM_EMAIL = get_env_variable("EMAIL_HOST_USER")  # noqa: F405
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_HOST_USER = get_env_variable("EMAIL_HOST_USER")  # noqa: F405
EMAIL_HOST_PASSWORD = get_env_variable("EMAIL_HOST_PASSWORD")  # noqa: F405
EMAIL_SUBJECT_PREFIX = get_env_variable("EMAIL_SUBJECT_PREFIX")  # noqa: F405
EMAIL_PORT = get_env_variable("EMAIL_PORT")  # noqa: F405
EMAIL_USE_TLS = True
