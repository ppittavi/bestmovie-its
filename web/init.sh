#!/bin/bash

echo 'Launching the web container...\n\n'

echo 'Migrating data schema'
python manage.py migrate

if [ ! -f /projectstatus/init.flag ]; then
    echo "Initializing the project"


    echo 'Creating a superuser for this project'
    # --preserve: Exit normally if the user already exists.
    python manage.py createsuperuser2 --username admin --password admin --email foo@example.com --noinput --preserve

    # save init flag
    date > /projectstatus/init.flag

else
    echo "The project is already initialized. No more operations are necessary"
fi


if [ "$PRODUCTION" = True ]; then

    echo '\n'
    echo ' *****   *****      * *     * *       +'
    echo ' *    *  *    *   *     *   *    *    +'
    echo ' * **    * **     *     *   *    *    +'
    echo ' *       *  *     *     *   *    *     '
    echo ' *       *    *     * *     * * *     +\n\n'

    echo 'Copying Django static files for deployment!'
    python manage.py collectstatic --noinput

    echo 'Starting gunicorn to serve the Project in PRODUCTION'
    gunicorn --workers=4 --bind 0.0.0.0:8000 bestmovie.wsgi

else
    echo 'Starting development server'
    python manage.py runserver 0.0.0.0:8000
fi
