from django.contrib import admin

from .models import Movie, Profile, ProfileStatus

# Register your models here.


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ("MovieID", "MovieTitle", "MovieGenre")


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass


@admin.register(ProfileStatus)
class ProfileStatusAdmin(admin.ModelAdmin):
    pass
