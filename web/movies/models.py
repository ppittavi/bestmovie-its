from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Movie(models.Model):
    MovieID = models.PositiveIntegerField(unique=True)
    MovieTitle = models.CharField(max_length=240, null=True)
    MovieGenre = models.CharField(max_length=240, null=True)
    MovieImage = models.URLField(max_length=240, null=True)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    biography = models.CharField(max_length=240, null=True)
    city = models.CharField(max_length=40, null=True)

    def __str__(self):
        return self.user.username


class ProfileStatus(models.Model):
    user_profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    status_content = models.CharField(max_length=240)
    created_at = models.DateTimeField(auto_now_add=True)
    udpated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "statuses"

    def __str__(self):
        return self.user_profile
