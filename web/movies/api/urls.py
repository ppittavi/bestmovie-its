from django.urls import path
from movies.api.views import AuthListAPIView, MovieDetailAPIView, MovieListCreateAPIView

urlpatterns = [
    path("movie/", MovieListCreateAPIView.as_view(), name="movie-list"),
    path("movie/<int:pk>/", MovieDetailAPIView.as_view(), name="movie-detail"),
    path("need_auth/movie/", AuthListAPIView.as_view(), name="auth-movie-list"),
    #    path('movie/', movie_list_api_view, name="movie-list")
]
